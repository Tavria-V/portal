# config valid only for current version of Capistrano
lock '3.6.1'

set :stages, ["production"]
set :default_stage, "production"
#set :deploy_via, :remote_cache

set :application, 'portal'
#set :scm, :git
set :repo_url, '.'

set :with_submodules, false


#server "172.16.10.35", :web # your server address or IP
set :user, "deploy" # your username for ssh
set :use_sudo, false
set :branch, 'master'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
#set :deploy_to, '/var/www/portal'

# Default value for :scm is :git
set :scm, :git_copy


namespace :deploy do

    desc 'PRINT THE SERVER NAME'
    task :print_server_name do
        on roles(:app), in: :groups, limit:1 do
        execute "hostname"
        end
    end
    task :composer do
        on roles(:app) do
          within release_path do
            execute "cd #{release_path} && composer global require 'fxp/composer-asset-plugin:~1.2.0'"
            # execute "cd #{release_path} && composer install"
            execute "cd #{release_path} && composer update --prefer-dist"
          end
        end
    end

end

set :linked_dirs, %w(frontend/web/uploads/ckeditor frontend/web/uploads frontend/web/uploads/attachments frontend/web/assets/thumbnails)

after "deploy:updated", "deploy:composer"
after "deploy:updated", "deploy:print_server_name"