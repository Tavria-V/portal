<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data'] // important
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ukr_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6 ],
        'preset' => 'standard',
        'clientOptions' => [
            'language'=>'ru',
            'filebrowserUploadUrl' => Url::to(['news/cke-upload']),
            'inline'=>true,
        ]
    ]) ?>

    <?= $form->field($model, 'ukr_content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6 ],
        'preset' => 'standard',
        'clientOptions' => [
            'language'=>'ru',
            'filebrowserUploadUrl' => Url::to(['news/cke-upload']),
            'inline'=>true,
        ]

    ]) ?>

    <?= $model->isNewRecord ? $form->field($model, 'image')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => false,],
        'pluginOptions'=>[
            'allowedFileExtensions'=>['jpg','gif','png'],
            'showUpload' => false,
            'initialPreview' => [
                $model->image ? Html::img(Yii::getAlias('@frontendUploads').'/'.$model->image, ['class'=>'file-preview-image', 'alt'=>$model->title, 'title'=>$model->title]) : null,
            ],
            'overwriteInitial'=> true,
        ],
    ]) : $form->field($model, 'imageUp')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => false,],
        'pluginOptions'=>[
            'allowedFileExtensions'=>['jpg','gif','png'],
            'showUpload' => false,
            'initialPreview' => [
                $model->image ? Html::img(Yii::getAlias('@frontendUploads').'/'.$model->image, ['class'=>'file-preview-image', 'alt'=>$model->title, 'title'=>$model->title]) : null,
            ],
            'overwriteInitial'=> true,
        ],
    ]) ?>


    <?= $form->field($model, 'status')->dropDownList([
        '0' => 'Не опубликованная',
        '1' => 'Опубликованная'
    ]) ?>
    <?= $form->field($model, 'priority')->dropDownList([
        '0' => 'Не задано',
        '1' => 'Топ'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>