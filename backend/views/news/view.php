<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\StringHelper;
/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'ukr_title',
            [
                'attribute' => 'content',
                'format' => 'text',
                'value' => call_user_func( function ($model) {
                    return StringHelper::truncate($model->content, 200);
                }, $model),
            ],
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => call_user_func( function ($data) {
                    return Html::img(Yii::getAlias('@frontendUploads').'/'. $data['image'],
                        ['width' => '100px']);
                }, $model),
            ],
            'create_date',
            [
                'attribute'=>'status',
                'value'=>call_user_func(function($data){
                    return ($data->status==1 ? "Опубликованная" : "Не опубликованная");
                },$model),
            ],
        ],
    ]) ?>

</div>