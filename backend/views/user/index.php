<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             'email:email',
            [
                'attribute'=>'roles',
                'value'=>function($data){
                    $result = 'Гость';
                    switch ($data->roles) {
                        case '1':
                            $result = 'Клиент';
                            break;
                        case '5':
                            $result = 'Сотрудник';
                            break;
                        case '10':
                            $result = 'Администратор';
                            break;
                    }
                        return $result;
                }
            ],
             'department',
             'phone',
             'fio',
            [
                'attribute'=>'position',
                'value'=>function($data){
                    $result = 'Гость';
                    switch ($data->position) {
                        case 'staff':
                            $result = 'Сотрудник';
                            break;
                        case 'manager':
                            $result = 'Менеджер';
                            break;
                        case 'merchandiser':
                            $result = 'Мерчендайзер';
                            break;
                    }
                    return $result;
                }
            ],
            [
                'attribute'=>'status',
                'value'=>function($data){
                    return ($data->status==10 ? "Активный" : "Не Активный");
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],

        ],
    ]); ?>
</div>
