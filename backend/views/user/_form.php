<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use common\components\CustomSelect2;
/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

$url = \yii\helpers\Url::to(['client-list']);
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
    if(Yii::$app->user->can('admin')){?>
        <?= $form->field($model, 'email')->textInput() ?>
        <?= $form->field($model, 'roles')->dropDownList(['1'=>'Клиент','5'=>'Сотрудник','10'=>'Администратор']); ?>
        <?= $form->field($model, 'department')->textInput() ?>
        <?= $form->field($model, 'phone')->textInput() ?>
        <?= $form->field($model, 'fio')->textInput() ?>
        <?= $form->field($model, 'position')->dropDownList(['staff'=>'Сотрудник', 'manager'=>'Менеджер','merchandiser'=>'Мерчендайзер']) ?>
        <?php
        if(isset($USER_COMPANIES)){

            $tags=[];
            foreach ($USER_COMPANIES as $value){
                $NAME = $value['company_name'];
                $ID = $value['company_code'];
                $tags[$NAME]=$ID;
            }
            $model->companies = $tags;
        }

        echo $form->field($model, 'companies')->widget(CustomSelect2::classname(), [
            'options' => [
                'multiple' => true,
                'placeholder' => 'Название...'
            ],
            'pluginOptions' => [
                'minimumInputLength' => 3,
                'tags' => true,
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(client) { return client.text; }'),
                'templateSelection' => new JsExpression('function (client) { return client.text; }'),
                'theme'=>'bootstrap',
            ],
        ])->label('Компания');
        ?>
        <?= $form->field($model, 'status')->dropDownList(['10'=> 'Активный', '0'=>'Не активный']) ?>

        <?php

    }else{?>

        <?= $form->field($model, 'email')->textInput() ?>
        <?= $form->field($model, 'roles')->dropDownList(['1'=>'Клиент','5'=>'Сотрудник'],['disabled'=> 'disabled']); ?>
        <?= $form->field($model, 'department')->textInput() ?>
        <?= $form->field($model, 'phone')->textInput() ?>
        <?= $form->field($model, 'fio')->textInput() ?>
        <?= $form->field($model, 'position')->dropDownList(['manager'=>'Менеджер','merchandiser'=>'Мерчендайзер']) ?>

        <?= $form->field($model, 'status')->dropDownList(['10'=> 'Активный', '0'=>'Не активный']) ?>
        <?php
        //представление показывающее пользователю ошибку
    }
    ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
