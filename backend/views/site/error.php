<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Произошла ошибка запроса. Возможно что-то пошло не так (
    </p>
    <p>
            Пожалуйста, свяжитесь с системным администратором ТАВРИЯ В.
    </p>

</div>
