<?php

namespace backend\controllers;

use common\models\UserCompanies;
use Yii;
use common\models\User;
use common\models\IStreamClient;
use common\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],

            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions'=>['login','error'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions'=>['index','update','create','delete','view','client-list'],
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => true,
                        'actions'=>['index','create','update','view',],
                        'roles' => ['moder'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['user'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function generatePassword(){
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }


    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new User();
        $model->auth_key = Yii::$app->security->generateRandomString();
        $password = $this->generatePassword();
        $model->password = $password;
        $model->username = $model->email;

        $userPOST = Yii::$app->request->post('User');
        $model->companies= $userPOST['companies'];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'createUser-html', 'text' => 'createUser-text'],
                    ['user' => $model, 'password' => $password]
                )
                ->setFrom([Yii::$app->params['supportEmail']])
                ->setTo($model->email)
                ->setSubject('Для вас создана учетная запись. ТАВРИЯ В')
                ->send();

            return $this->redirect(['view', 'id' => $model->id]);

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */  
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $userPOST = Yii::$app->request->post('User');


        $cmp_name = $userPOST['companies'];
        $model->companies = $cmp_name;
        $USER_COMPANIES = new UserCompanies();
        $usr_cmp =$USER_COMPANIES->find()->where(['user_id'=> $id])->asArray(true)->all();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model, 'USER_COMPANIES'=>$usr_cmp
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        \Yii::$app
            ->db
            ->createCommand()
            ->delete('user_companies', ['user_id' => $id])
            ->execute();
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }

    /*************
     * Controller AJAX Auto complete
     ************/
    public function actionClientList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {

            $client = IStreamClient::find()->select(['ID_CLIENT','DESCR'])->where(['like','DESCR',$q])->all();
            $out=[];
            foreach ($client as $c) 
            {
                $out['results'][]=['id'=>$c->ID_CLIENT,'text'=>$c->DESCR];
            }

        }

        return $out;
    }

}
