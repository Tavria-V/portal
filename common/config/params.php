<?php
return [
    'adminEmail' => 's.semenov@tavriav.com.ua',
    'supportEmail' => 's.semenov@tavriav.com.ua',
    'user.passwordResetTokenExpire' => 3600,
    'uploadPath' => '@frontend/web/uploads/',
];
