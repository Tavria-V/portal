<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=customer_portal_db',
            'username' => 'root',
            'password' => 'batva',
            'charset' => 'utf8',
        ], 
        'olap'=>[
            'class' => 'yii\db\Connection',
            'dsn' => 'dblib:host=egServer70;dbname=PostavQueries;charset=UTF-8',
            'username' => 'istream',
            'password' => 'istream2016',
        ],
        'OlapSrv'=>[
            'class' => 'yii\db\Connection',
            'dsn' => 'dblib:host=egServer70;dbname=skladolap;charset=UTF-8',
            'username' => 'rekk',
            'password' => 'rekk',
        ],
        'OlapCost'=>[
            'class' => 'yii\db\Connection',
            'dsn' => 'dblib:host=egServer70;dbname=Server Cost;charset=UTF-8',
            'username' => 'rekk',
            'password' => 'rekk',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'thumbnail' => [
            'class' => 'himiklab\thumbnail\EasyThumbnail',
            'cacheAlias' => 'assets/gallery_thumbnails',
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['user','moder','admin'], //здесь прописываем роли
            //зададим куда будут сохраняться наши файлы конфигураций RBAC
            'itemFile' => '@common/components/rbac/items.php',
            'assignmentFile' => '@common/components/rbac/assignments.php',
            'ruleFile' => '@common/components/rbac/rules.php'
        ],
    ],

    // set target language to be Russian
    'language' => 'ru-RU',
];
