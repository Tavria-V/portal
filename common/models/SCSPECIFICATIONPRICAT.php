<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "SC_SPECIFICATION_PRICAT".
 *
 * @property string $Code_PRODUCT
 * @property string $ID_POSTAV
 * @property double $Price_New
 * @property string $Created
 * @property string $DateFrom
 */
class SCSPECIFICATIONPRICAT extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SC_SPECIFICATION_PRICAT';
    }
    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('OlapCost');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Code_PRODUCT', 'ID_POSTAV', 'Price_New', 'Created', 'DateFrom'], 'required'],
            [['Code_PRODUCT', 'ID_POSTAV'], 'string'],
            [['Price_New'], 'number'],
            [['Created', 'DateFrom'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Code_PRODUCT' => 'Code  Product',
            'ID_POSTAV' => 'Id  Postav',
            'Price_New' => 'Новая Цена',
            'Created' => 'Created',
            'DateFrom' => 'Дата',
        ];
    }
}
