<?php

namespace common\models;
use common\models\IStreamClient;
use common\models\iStreamStore;
use Yii;

/**
 * This is the model class for table "user_companies".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $company_code
 * @property string $company_name
 */
class UserCompanies extends \yii\db\ActiveRecord
{

    public $companies = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_companies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['company_code'], 'string', 'max' => 255],
            [['CODE'], 'string', 'max' => 255],
            [['company_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'company_code' => 'Company Code',
            'CODE' => 'CODE',
            'company_name' => 'Company Name',
        ];
    }

    public function getStores()
    {
        return $this->hasOne(IStreamStore::className(), ['CODE' => 'CODE']);
    }

    public function getClients()
    {
        return $this->hasOne(IStreamClient::className(), ['CODE' => 'CODE']);
    }

}
