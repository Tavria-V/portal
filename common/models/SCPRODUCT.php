<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "SC_PRODUCT".
 *
 * @property string $ID_PRODUCT
 * @property integer $ID_PRODUCT_int
 * @property string $PARENTID
 * @property integer $PARENTID_int
 * @property string $CODE
 * @property string $DESCR
 * @property integer $ISFOLDER
 * @property integer $TreeLevel
 * @property string $ID_MODE_PRODUCT
 * @property string $MODE_OF_GOODS
 * @property string $BARCODE
 * @property string $PRICE_PURCHASE
 * @property string $PRICE_INFORM
 * @property string $Weight
 * @property string $ID_PRODUCER
 * @property string $ID_COMPANY
 * @property string $ID_MANAGER
 * @property integer $ID_SET
 * @property string $MARK_GOODS
 * @property string $NUM_PACKAGE
 * @property string $NUM_PACK_PALLET
 * @property string $DATE__FSALE
 * @property integer $DATE_ID
 * @property string $SkladCex
 * @property string $TypeProd
 * @property string $Kategoria
 * @property string $ExchGroup
 * @property double $ItemKoeff
 * @property integer $ApolloProd
 * @property string $Status
 * @property string $ID_BREND
 * @property string $WMS_Group
 * @property integer $WMS_Group_int
 * @property integer $WMS_ProdItem
 * @property double $weight_netto
 * @property string $type_upravleniya
 * @property string $is_GOLD
 */
class SCPRODUCT extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SC_PRODUCT';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('OlapSrv');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PRODUCT', 'ID_PRODUCT_int', 'PARENTID', 'PARENTID_int', 'CODE', 'DESCR', 'ISFOLDER', 'TreeLevel', 'ID_MODE_PRODUCT', 'MODE_OF_GOODS', 'PRICE_PURCHASE', 'PRICE_INFORM', 'Weight', 'ID_PRODUCER', 'ID_COMPANY', 'ID_SET', 'MARK_GOODS', 'NUM_PACKAGE', 'NUM_PACK_PALLET', 'DATE__FSALE', 'DATE_ID', 'WMS_ProdItem'], 'required'],
            [['ID_PRODUCT', 'PARENTID', 'CODE', 'DESCR', 'ID_MODE_PRODUCT', 'BARCODE', 'ID_PRODUCER', 'ID_COMPANY', 'MARK_GOODS', 'SkladCex', 'TypeProd', 'Kategoria', 'ExchGroup', 'Status', 'ID_BREND', 'WMS_Group', 'type_upravleniya', 'is_GOLD'], 'string'],
            [['ID_PRODUCT_int', 'PARENTID_int', 'ISFOLDER', 'TreeLevel', 'ID_SET', 'DATE_ID', 'ApolloProd', 'WMS_Group_int', 'WMS_ProdItem'], 'integer'],
            [['MODE_OF_GOODS', 'PRICE_PURCHASE', 'PRICE_INFORM', 'Weight', 'ID_MANAGER', 'NUM_PACKAGE', 'NUM_PACK_PALLET', 'ItemKoeff', 'weight_netto'], 'number'],
            [['DATE__FSALE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_PRODUCT' => 'Id  Product',
            'ID_PRODUCT_int' => 'Id  Product Int',
            'PARENTID' => 'Parentid',
            'PARENTID_int' => 'Parentid Int',
            'CODE' => 'Code',
            'DESCR' => 'Descr',
            'ISFOLDER' => 'Isfolder',
            'TreeLevel' => 'Tree Level',
            'ID_MODE_PRODUCT' => 'Id  Mode  Product',
            'MODE_OF_GOODS' => 'Mode  Of  Goods',
            'BARCODE' => 'Barcode',
            'PRICE_PURCHASE' => 'Price  Purchase',
            'PRICE_INFORM' => 'Price  Inform',
            'Weight' => 'Weight',
            'ID_PRODUCER' => 'Id  Producer',
            'ID_COMPANY' => 'Id  Company',
            'ID_MANAGER' => 'Id  Manager',
            'ID_SET' => 'Id  Set',
            'MARK_GOODS' => 'Mark  Goods',
            'NUM_PACKAGE' => 'Num  Package',
            'NUM_PACK_PALLET' => 'Num  Pack  Pallet',
            'DATE__FSALE' => 'Date   Fsale',
            'DATE_ID' => 'Date  ID',
            'SkladCex' => 'Sklad Cex',
            'TypeProd' => 'Type Prod',
            'Kategoria' => 'Kategoria',
            'ExchGroup' => 'Exch Group',
            'ItemKoeff' => 'Item Koeff',
            'ApolloProd' => 'Apollo Prod',
            'Status' => 'Status',
            'ID_BREND' => 'Id  Brend',
            'WMS_Group' => 'Wms  Group',
            'WMS_Group_int' => 'Wms  Group Int',
            'WMS_ProdItem' => 'Wms  Prod Item',
            'weight_netto' => 'Weight Netto',
            'type_upravleniya' => 'Type Upravleniya',
            'is_GOLD' => 'Is  Gold',
        ];
    }
}
