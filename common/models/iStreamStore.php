<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "iStreamStore".
 *
 * @property string $ID_CLIENT
 * @property string $CODE
 * @property string $DESCR
 * @property string $ADDRESS
 * @property string $GLN
 */
class iStreamStore extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'iStreamStore';
    }
    public static function primaryKey()
    {
        return ['ID_CLIENT'];
    }
    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('olap');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_CLIENT', 'CODE', 'DESCR', 'ADDRESS', 'GLN'], 'required'],
            [['ID_CLIENT', 'CODE', 'DESCR', 'ADDRESS', 'GLN'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_CLIENT' => 'Id  Client',
            'CODE' => 'Code',
            'DESCR' => 'Descr',
            'ADDRESS' => 'Address',
            'GLN' => 'Gln',
        ];
    }
}
