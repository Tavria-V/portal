<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $ukr_title
 * @property string $content
 * @property string $ukr_content
 * @property string $image
 * @property string $create_date
 * @property integer $status
 * @property integer $priority
 * @property integer $author_id
 */
class News extends \yii\db\ActiveRecord
{
    public $imageUp;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'ukr_content'], 'string'],
            [['create_date', 'image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
            [['status', 'author_id', 'priority'], 'integer'],
            [['title','content','image'], 'required'],
            [['title', 'ukr_title'], 'string', 'max' => 255],
        ];
    }


    public function beforeSave($insert)
    {
        $this->author_id = Yii::$app->user->id;
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'ukr_title' => 'Укр.Заголовок',
            'content' => 'Контент',
            'ukr_content' => 'Укр.Контент',
            'image' => 'Обложка',
            'imageUp' => 'Обложка',
            'create_date' => 'Дата создания',
            'status' => 'Статус',
            'author_id' => 'Author ID',
            'priority' => 'Приоритет',
        ];
    }
}
