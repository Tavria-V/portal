<?php

namespace common\models;

use Yii;
use common\models\iStreamStore;
/**
 * This is the model class for table "REMAINDER_CUR".
 *
 * @property string $IDDOC
 * @property integer $LINENO_
 * @property string $ID_PRODUCT
 * @property double $QUANTITY
 * @property string $ID_ITEM
 * @property double $COEFFICIENT
 * @property double $SUM_CUR
 * @property double $SUM_GRN
 * @property string $ID_COMPANY
 * @property string $DOC_DATE
 * @property string $DOC_NUM
 * @property double $PRICE_GRN
 * @property string $ID_STORE
 * @property string $ID_FIRM
 * @property integer $DATE_ID
 * @property integer $DATE_ID_FRM
 * @property string $DOC_DATE_FRM
 * @property string $ACTION_ID
 * @property double $QUANT_Real
 * @property integer $CodeProdStore
 */
class REMAINDERCUR extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'REMAINDER_CUR';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('OlapSrv');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IDDOC', 'LINENO_', 'ID_PRODUCT', 'QUANTITY', 'ID_ITEM', 'COEFFICIENT', 'SUM_CUR', 'SUM_GRN', 'ID_COMPANY', 'DOC_DATE', 'DOC_NUM', 'PRICE_GRN', 'ID_STORE', 'ID_FIRM', 'DATE_ID', 'DATE_ID_FRM', 'DOC_DATE_FRM', 'ACTION_ID', 'QUANT_Real'], 'required'],
            [['IDDOC', 'ID_PRODUCT', 'ID_ITEM', 'ID_COMPANY', 'DOC_NUM', 'ID_STORE', 'ID_FIRM', 'ACTION_ID'], 'string'],
            [['LINENO_', 'DATE_ID', 'DATE_ID_FRM', 'CodeProdStore'], 'integer'],
            [['QUANTITY', 'COEFFICIENT', 'SUM_CUR', 'SUM_GRN', 'PRICE_GRN', 'QUANT_Real'], 'number'],
            [['DOC_DATE', 'DOC_DATE_FRM'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IDDOC' => 'Iddoc',
            'LINENO_' => 'Lineno',
            'ID_PRODUCT' => 'Id  Product',
            'QUANTITY' => 'Quantity',
            'ID_ITEM' => 'Id  Item',
            'COEFFICIENT' => 'Coefficient',
            'SUM_CUR' => 'Sum  Cur',
            'SUM_GRN' => 'Sum  Grn',
            'ID_COMPANY' => 'Id  Company',
            'DOC_DATE' => 'Doc  Date',
            'DOC_NUM' => 'Doc  Num',
            'PRICE_GRN' => 'Price  Grn',
            'ID_STORE' => 'Id  Store',
            'ID_FIRM' => 'Id  Firm',
            'DATE_ID' => 'Date  ID',
            'DATE_ID_FRM' => 'Date  Id  Frm',
            'DOC_DATE_FRM' => 'Doc  Date  Frm',
            'ACTION_ID' => 'Action  ID',
            'QUANT_Real' => 'Quant  Real',
            'CodeProdStore' => 'Code Prod Store',
        ];
    }

    public function getProduct(){
        return $this->hasOne(SCPRODUCT::className(), ['ID_PRODUCT' => 'ID_PRODUCT']);
    }

    public function getShop(){
        return $this->hasOne(iStreamStore::className(), ['ID_CLIENT' => 'ID_STORE']);
    }

}
