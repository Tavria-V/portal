<?php

namespace common\models;

use Yii;
use common\models\iStreamStore;
/**
 * This is the model class for table "SALES_DETAIL_CHILD".
 *
 * @property string $IDDOC
 * @property integer $LINENO_
 * @property string $ID_PRODUCT
 * @property double $QUANTITY
 * @property string $ID_ITEM
 * @property double $COEFFICIENT
 * @property double $PRICE
 * @property double $SUM_PROD
 * @property double $VAT
 * @property double $SUM_CUR
 * @property string $ID_COMPANY
 * @property double $DISCOUNT_PRICE
 * @property double $DISCOUNT_SUM
 * @property string $DOC_DATE
 * @property string $DOC_NUM
 * @property integer $DATE_ID
 * @property integer $DateSale
 * @property string $ID_STORE
 * @property string $ID_FIRM
 * @property string $ID_SHOP
 * @property integer $ID_TYPE_TRADE
 * @property integer $ID_MODE_INVOICE
 * @property double $PROFIT
 * @property string $ACTN
 * @property integer $ID_WRT_OFF
 * @property integer $DATE_IDF
 * @property string $ID_DISCOUNT
 * @property integer $codeProdStore
 * @property string $ID_BUYER
 * @property string $ID_STATUS
 * @property string $ID_Reserv_1
 * @property string $ID_Reserv_2
 * @property double $Val_1
 * @property double $Val_2
 * @property string $Val_3
 * @property string $Val_4
 * @property integer $Val_5
 * @property integer $Val_6
 * @property double $SumOfDebt
 */
class SALESDETAILCHILD extends \yii\db\ActiveRecord
{
    public $Qtty;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SALES_DETAIL_CHILD';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('OlapSrv');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IDDOC', 'LINENO_', 'ID_PRODUCT', 'QUANTITY', 'ID_ITEM', 'COEFFICIENT', 'PRICE', 'SUM_PROD', 'VAT', 'SUM_CUR', 'DOC_DATE', 'DOC_NUM', 'DATE_ID', 'DateSale', 'ID_STORE', 'ID_FIRM', 'ID_SHOP', 'ID_TYPE_TRADE', 'ID_MODE_INVOICE', 'PROFIT', 'ACTN', 'ID_WRT_OFF', 'DATE_IDF', 'ID_DISCOUNT'], 'required'],
            [['IDDOC', 'ID_PRODUCT', 'ID_ITEM', 'ID_COMPANY', 'DOC_NUM', 'ID_STORE', 'ID_FIRM', 'ID_SHOP', 'ACTN', 'ID_DISCOUNT', 'ID_BUYER', 'ID_STATUS', 'ID_Reserv_1', 'ID_Reserv_2'], 'string'],
            [['LINENO_', 'DATE_ID', 'DateSale', 'ID_TYPE_TRADE', 'ID_MODE_INVOICE', 'ID_WRT_OFF', 'DATE_IDF', 'codeProdStore', 'Val_5', 'Val_6'], 'integer'],
            [['QUANTITY', 'COEFFICIENT', 'PRICE', 'SUM_PROD', 'VAT', 'SUM_CUR', 'DISCOUNT_PRICE', 'DISCOUNT_SUM', 'PROFIT', 'Val_1', 'Val_2', 'Val_3', 'Val_4', 'SumOfDebt'], 'number'],
            [['DOC_DATE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IDDOC' => 'Iddoc',
            'LINENO_' => 'Lineno',
            'ID_PRODUCT' => 'Id  Product',
            'QUANTITY' => 'Quantity',
            'ID_ITEM' => 'Id  Item',
            'COEFFICIENT' => 'Coefficient',
            'PRICE' => 'Price',
            'SUM_PROD' => 'Sum  Prod',
            'VAT' => 'Vat',
            'SUM_CUR' => 'Sum  Cur',
            'ID_COMPANY' => 'Id  Company',
            'DISCOUNT_PRICE' => 'Discount  Price',
            'DISCOUNT_SUM' => 'Discount  Sum',
            'DOC_DATE' => 'Doc  Date',
            'DOC_NUM' => 'Doc  Num',
            'DATE_ID' => 'Date  ID',
            'DateSale' => 'Date Sale',
            'ID_STORE' => 'Id  Store',
            'ID_FIRM' => 'Id  Firm',
            'ID_SHOP' => 'Id  Shop',
            'ID_TYPE_TRADE' => 'Id  Type  Trade',
            'ID_MODE_INVOICE' => 'Id  Mode  Invoice',
            'PROFIT' => 'Profit',
            'ACTN' => 'Actn',
            'ID_WRT_OFF' => 'Id  Wrt  Off',
            'DATE_IDF' => 'Date  Idf',
            'ID_DISCOUNT' => 'Id  Discount',
            'codeProdStore' => 'Code Prod Store',
            'ID_BUYER' => 'Id  Buyer',
            'ID_STATUS' => 'Id  Status',
            'ID_Reserv_1' => 'Id  Reserv 1',
            'ID_Reserv_2' => 'Id  Reserv 2',
            'Val_1' => 'Val 1',
            'Val_2' => 'Val 2',
            'Val_3' => 'Val 3',
            'Val_4' => 'Val 4',
            'Val_5' => 'Val 5',
            'Val_6' => 'Val 6',
            'SumOfDebt' => 'Sum Of Debt',
        ];
    }

    public function getProduct(){
        return $this->hasOne(SCPRODUCT::className(), ['ID_PRODUCT' => 'ID_PRODUCT']);
    }

    public function getShop(){
        return $this->hasOne(iStreamStore::className(), ['ID_CLIENT' => 'ID_SHOP']);
    }


}
