<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "iStreamClient".
 *
 * @property string $ID_CLIENT
 * @property string $CODE
 * @property string $DESCR
 * @property string $FIO
 * @property string $ADDRESS
 * @property string $GLN
 * @property integer $Status
 * @property integer $DayAfterOrder
 */
class IStreamClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'iStreamClient';
    }

    public static function primaryKey()
    {
        return ['ID_CLIENT'];
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('olap');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_CLIENT', 'CODE', 'DESCR', 'FIO', 'ADDRESS', 'GLN'], 'required'],
            [['ID_CLIENT', 'CODE', 'DESCR', 'FIO', 'ADDRESS', 'GLN'], 'string'],
            [['Status', 'DayAfterOrder'], 'integer'],
        ];
    }
    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_CLIENT' => 'Id  Client',
            'CODE' => 'Code',
            'DESCR' => 'Descr',
            'FIO' => 'Fio',
            'ADDRESS' => 'Address',
            'GLN' => 'Gln',
            'Status' => 'Status',
            'DayAfterOrder' => 'Day After Order',
        ];
    }
}
