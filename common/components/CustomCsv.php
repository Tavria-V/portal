<?php
namespace common\components;
/**
 * This is just an example.
 */
class CustomCsv extends \yii\base\Widget
{
    /**
     * @var array Data rows
     */
    public $data = [];
    /**
     * @var bool Download csv
     */
    public $download = true;
    /**
     * @var string the directory where to save the csv file
     */
    public $saveDir = "";
    /**
     * @var string the resulting filename
     */
    public $filename = "export.csv";
    /**
     * Init widget
     */
    public function init(){
        parent::init();
        if(!$this->saveDir) $this->saveDir = \Yii::$app->basePath."/runtime";
        if(!file_exists($this->saveDir)) mkdir($this->saveDir, 0777, true);
    }
    /**
     * run Widget
     */
    public function run(){
        $filename = $this->filename;
        $res = $this->data;

        $campos = false;
        if($this->download){
            $fp = fopen('php://output', 'w');
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header( 'Content-Disposition: attachment;filename='.$filename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            echo "\xEF\xBB\xBF"; // UTF-8 BOM
        }else{
            $fp = fopen($this->saveDir."/".$this->filename, 'w');
        }

        foreach($res as $data){

            if(!$campos){
                $campos = array_keys($data);
                fputcsv($fp, $campos, ";");
            }
          //  $fila = array_map('utf8_decode', $data);
 
            fputcsv($fp, $data, ";");
        }
        fclose($fp);
    }
}