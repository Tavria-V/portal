<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$resetLink = 'https://portal.tavriav.info/site/reset-password?token='.$user->password_reset_token;
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Здравствуйте, <?= Html::encode($user->fio) ?> ! Мы создали для вас учетную запись на нашем портале.</p>
    <p>Для авторизации используйте ваш логин(<?= $user->email ?>) и пароль: <?= $password?> </p>
    <a href="https://portal.tavriav.info" alt="Портал поставщика">Портал поставщика. ТАВРИЯ В.</a>
</div>
