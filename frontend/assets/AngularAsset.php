<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AngularAsset extends AssetBundle
{
    public $sourcePath = '@frontend';
    public $jsOptions = ['position' => \yii\web\View::POS_BEGIN];

    public $css = [
        'angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $js = [
        'web/js/example.js',
        'web/js/helpers.js',
    ];
}
