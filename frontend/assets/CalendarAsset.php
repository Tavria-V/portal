<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CalendarAsset extends AssetBundle
{
    public $sourcePath = '@bower';
    public $jsOptions = ['position' => \yii\web\View::POS_BEGIN];

    public $css = [
        'angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $js = [
//        'angular-bootstrap-calendar/example.js',
//        'angular-bootstrap-calendar/helpers.js',
        'oclazyload/dist/ocLazyLoad.js',
        'angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar.min.js',
        'angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js',
    ];
}
