<?php

namespace app\models;
use common\models\IStreamClient;
use common\models\iStreamStore;
use Yii;

/**
 * This is the model class for table "iStream_schedule".
 *
 * @property string $code_store
 * @property string $Code_client
 * @property string $CODE
 * @property string $DateOrder
 * @property string $DateDelivery
 * @property integer $Status
 * @property string $Created
 * @property string $Updated
 * @property string $Order
 * @property string $DesAdv
 * @property integer $ID
 */
class IStreamSchedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'iStream_schedule';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('olap');
    }

    public static function primaryKey()
    {
        return ['ID'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_store', 'Code_client','CODE', 'DateOrder', 'DateDelivery', 'Status', 'Created'], 'required'],
            [['code_store', 'Code_client', 'Order', 'DesAdv'], 'string'],
            [['DateOrder', 'DateDelivery', 'Created', 'Updated'], 'safe'],
            [['Status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code_store' => 'Code Store',
            'Code_client' => 'Code Client',
            'CODE' => 'CODE',
            'DateOrder' => 'Date Order',
            'DateDelivery' => 'Date Delivery',
            'Status' => 'Status',
            'Created' => 'Created',
            'Updated' => 'Updated',
            'Order' => 'Order',
            'DesAdv' => 'Des Adv',
            'ID' => 'ID',
        ];
    }
    public function getStores()
    {
        return $this->hasOne(IStreamStore::className(), ['CODE' => 'code_store']);
    }

    public function getClients()
    {
        return $this->hasOne(IStreamClient::className(), ['CODE' => 'Code_client']);
    }
}
