<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IStreamSchedule;
 
/**
 * IStreamScheduleSearch represents the model behind the search form about `app\models\IStreamSchedule`.
 */
class IStreamScheduleSearch extends IStreamSchedule
{

    public $fio;
    public $descr;
    public $address;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code_store', 'Code_client', 'DateOrder', 'DateDelivery', 'Created', 'Updated', 'Order', 'DesAdv','fio','descr','address'], 'safe'],
            [['Status', 'ID'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'Status' => 'Статус',
            'DateOrder' => 'Дата Отправки',
            'DateDelivery' => 'Дата Поставки',
            'fio' => 'Клиент',
            'descr' => 'Склад / Магазин',
            'address' => 'Адрес (Магазин)',
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IStreamSchedule::find();
        $query->joinWith(['clients','stores']);
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dateO=date('M j Y ',strtotime($this->DateOrder));
        $dateF=date('M j Y ',strtotime($this->DateDelivery));
        $dateO=trim(iconv("utf-8", "windows-1251//TRANSLIT//IGNORE", $dateO));
        $dateF=trim(iconv("utf-8", "windows-1251//TRANSLIT//IGNORE", $dateF));

        // grid filtering conditions
        $query->andFilterWhere([

//            'DateOrder' => $this->DateOrder,
//            'DateDelivery' => $this->DateDelivery,
            'iStream_schedule.Status' => $this->Status,
            'Created' => $this->Created,
            'Updated' => $this->Updated,
            'ID' => $this->ID,
        ]);
        
        $descr=trim(iconv("utf-8", "windows-1251//TRANSLIT//IGNORE", $this->descr));
        $fio=trim(iconv("utf-8", "windows-1251//TRANSLIT//IGNORE", $this->fio));
        $address=trim(iconv("utf-8", "windows-1251//TRANSLIT//IGNORE", $this->address));

        $query->andFilterWhere(['like', 'iStreamStore.ID_CLIENT', $this->code_store])
            ->andFilterWhere(['like', 'iStreamClient.ID_CLIENT', $this->Code_client])
            ->andFilterWhere(['like', 'DateOrder', ($this->DateOrder ? $dateO  : null)])
            ->andFilterWhere(['like', 'DateDelivery', ($this->DateDelivery ? $dateF  : null)])
            ->andFilterWhere(['like', 'Order', $this->Order])
            ->andFilterWhere(['like', 'DesAdv', $this->DesAdv])
            ->andFilterWhere(['like', 'iStreamClient.FIO', $fio])
            ->andFilterWhere(['like', 'iStreamStore.ADDRESS', $address])
            ->andFilterWhere(['like', 'iStreamStore.DESCR', $descr]);

        return $dataProvider;
    }
}