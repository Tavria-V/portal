<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * CooperateForm is the model behind the contact form.
 */
class CooperateForm extends Model
{
    public $name;
    public $position;
    public $email;
    public $phone;
    public $company;
    public $message;
    public $files;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','position','phone','company'],'required'],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            // email has to be a valid email address
            [['files'], 'file', 'skipOnEmpty' => false],
        ];
    }

    public function upload(){
        if ($this->validate()) {
            $FILE = time().'_'.hash('crc32',$this->files->baseName) . '.' . $this->files->extension;
            $this->files->saveAs('uploads/attachments/'.$FILE);
            return Yii::$app->mailer->compose()
                ->setFrom([$this->email => $this->name])
                ->setTo('n.shapovalov@tavriav.com.ua')
                ->setSubject('Форма сотрудничества(Портал поставщика). От '.$this->name)
                ->setHtmlBody('
                            <h4>Заявка на сотрудничество с категорийным менеджером</h4>
                            <p><b>ФИО:</b> '.$this->name.'</p>
                            <p><b>Должность:</b> '.$this->position.'</p>
                            <p><b>Компания:</b> '.$this->company.'</p>
                            <p><b>Email:</b> '.$this->email.'</p>
                            <p><b>Телефон:</b> '.$this->phone.'</p>
                            <p>'.$this->message.'</p>
                ')
                ->attach(Yii::getAlias('@frontend').'/web/uploads/attachments/'.$FILE)
                ->send();

        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'name'=> 'ФИО',
            'position'=> 'Должность',
            'email'=>'Электронная почта',
            'phone'=>'Телефон',
            'company'=>'Компания',
            'files'=>'Анкета'
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {

        return Yii::$app->mailer->compose()
            ->setFrom(['porta@example.com' => 'sample mail'])
            ->setTo('m.eremenchuk@tavriav.com.ua')
            ->setSubject('Форма сотрудничества. От'.$this->name)
            ->setHtmlBody($this->message)
            ->send();

//
//
//        return Yii::$app->mailer->compose()
//            ->setTo('mishaerem@gmail.com')
//            ->setFrom($email)
//            ->setSubject('Форма сотрудничества. От'.$this->name)
//            ->setTextBody($this->message)
//            ->attach('@web/uploads/'.$this->file)
//            ->send();
    }
}
