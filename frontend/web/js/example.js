angular.module('mwl.calendar.docs', ['mwl.calendar', 'ngAnimate', 'ui.bootstrap', 'oc.lazyLoad']);

var app = angular.module('mwl.calendar.docs');

app.controller('KitchenSinkCtrl', function(moment, alert, $scope, $window, $http, calendarConfig, $ocLazyLoad) {

    var vm = this;

    vm.calendarView = 'month';
    vm.viewDate = new Date();

    $http.get('/site/get-data').
    success(function(data) {
        vm.events = [];
        var elements = angular.fromJson(data);
        angular.forEach(elements, function (value,key) {
            var event = [];
            dateStart = new Date(value.startsAt);
            dateEnd = new Date(value.endsAt);
            event['ID'] = value.ID;
            event['title'] = value.title;
            event['startsAt'] = dateStart;
            event['endsAt'] = dateEnd;
            event['color'] = calendarConfig.colorTypes[value.type];
            event['draggable'] = value.draggable;
            event['resizable'] = value.resizable;
            vm.events.push(event);
        });

    }).
    error(function(data) {
        // log error
    });

    calendarConfig.dateFormatter = 'moment'; // use moment instead of angular for formatting dates
    var originali18n = angular.copy(calendarConfig.
        i18nStrings);
    calendarConfig.i18nStrings.weekNumber = 'Неделя {week}';

    $window.moment = $window.moment || moment;
    $ocLazyLoad.load('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/ru.js').then(function() {
        moment.locale('ru', {
            week: {
                dow: 1 // Monday is the first day of the week
            }
        });
        moment.locale('ru'); // change the locale to french
    });

    $scope.$on('$destroy', function() {
        moment.locale('en');
        calendarConfig.i18nStrings = originali18n;
    });

    vm.isCellOpen = true;


    vm.toggle = function($event, field, event) {
        $event.preventDefault();
        $event.stopPropagation();
        event[field] = !event[field];
    };


    $scope.filtered = function () {

        var filterParams = $('#filter-form').serialize();

        $http.get('/site/filter-check?'+filterParams).success(function(data) {
            vm.events = [];
            var wmsEol = angular.fromJson(data);
                    angular.forEach(wmsEol, function (value,key) {
                        var event = [];
                        dateStart = new Date(value.startsAt);
                        dateEnd = new Date(value.endsAt);
                        event['ID'] = value.ID;
                        event['title'] = value.title;
                        event['startsAt'] = dateStart;
                        event['endsAt'] = dateEnd;
                        event['color'] = calendarConfig.colorTypes[value.type];
                        event['draggable'] = true;
                        event['resizable'] = value.resizable;
                        vm.events.push(event);
                    }); 

        }).
        error(function(data) {
            // log error
        });
    };

});



/**
 * Created by misha on 22.06.16. //
 */
