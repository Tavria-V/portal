<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\Pjax;
?>
<div class="user-view internal-page">
    <h2 class="common-title">
         Журнал заказов
    </h2> 
    <?php
    if(isset($company_ids)){?>
        <p>
            <b>
                Доступные поставщики:
            </b>
        </p>
        <div class="list-group specifications-cmp">
            <?php
            $active = Yii::$app->request->get('company_code');
            foreach ($company_ids as $company){
                if($active == $company['CODE']){
                    echo '<a href="'.Url::to(Url::home().'site/get-orders-info?company_code='.$company['CODE']).'" class="list-group-item list-group-item-action active"> <i class="glyphicon glyphicon-folder-open"></i> '.$company['company_name'].'</a>';
                }else{
                    echo '<a href="'.Url::to(Url::home().'site/get-orders-info?company_code='.$company['CODE']).'" class="list-group-item list-group-item-action"> <i class="glyphicon glyphicon-folder-open"></i> '.$company['company_name'].'</a>';
                }
            } ?>
        </div>
    <?php }?>
    <div class="">
        <!-- Modal -->
        <div class="modal fade" id="DetailView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Детальный обзор</h4>
                    </div>
                    <div class="modal-body table-responsive">
                        <?php Pjax::begin(['id' =>'pjaxModel',
                            'enablePushState' => false,
                            'linkSelector' => '.view-report a.getview',
                            'timeout'=> 5000
                        ]);
                        ?>
                        <?= $this->render('_detail');?>
                        <?php Pjax::end(); ?>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>



            <?php if(isset($document) && !empty($document)){ $min_date = date('Y-m-d', strtotime('-7 days')); $max_date = date('Y-m-d');?>
                <p class="period">
                    Период с <b><?= $min_date;?></b> по <b><?= $max_date;?></b>
                </p>

                <table class="table table-striped table-bordered balance">
                    <thead class="thead-inverse">
                    <tr>
                        <th>Номер</th>
                        <th class="store">Склад</th>
                        <th class="date">Дата заказа</th>
                        <th>Статус</th>
                    </tr>
                    </thead>
                    <tbody>

                        <?php
                        foreach ($document as $d){
                            if(!$d['ACTION']){
                                $d['ACTION'] = 'НЕ ПОДТВЕРЖДЕН';
                            }
                            echo '<tr>';
                            echo '<td>'.$d['FileName'].'</td>';
                            echo '<td class="store">'.$d['DESCR'].'</td>';
                            echo '<td class="date">'.$d['CREATED'].'</td>';
                            echo '<td style="background-color: #ee3637; color:white;">'.$d['ACTION'].'</td>';
                            echo '</tr>';
                        }
                    ?>

                    </tbody>
                </table>
            <?php } ?>
    </div>
    <p>
        Уважаемые партнеры, направляйте ваши коммерческие предложения по инвестициям и дополнительным условиям работы на электронную почту категорийному менеджеру компании "ТАВРИЯ В", который ведет вашу группу товаров.
    </p>
    <br/>
</div>
