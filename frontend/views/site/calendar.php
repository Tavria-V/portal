<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\IStreamScheduleSearch;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\select2\Select2;

$url = \yii\helpers\Url::to(['client-list']);
$clientDesc = empty($model->DESCR) ? '' : IStreamScheduleSearch::findOne($model->DESCR);
?>
<div class="user-view internal-page">

            <h2 class="common-title">
                Календарь поставок
            </h2>
        <p>
    <div ng-controller="KitchenSinkCtrl as vm" class="well">
        <?php $form = ActiveForm::begin(['id'=>'filter-form']);?>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>
                    <?php
                    echo $form->field($searchModel, 'Code_client')->widget(Select2::classname(), [

                        'initValueText' => $clientDesc, // set the initial display text
                        'options' => ['placeholder' => 'Клиент...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Идет поиск...'; }"),
                                'inputTooShort'=> new JsExpression("function() { return 'Введите первые 3 буквы'; }"),
                            ],
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(client) { return client.text; }'),
                            'templateSelection' => new JsExpression('function (client) { return client.text; }'),
                            'theme'=>'bootstrap',
                        ],
                    ])->label('Название поставщика:');
                    ?>
                </th>

                <th>
                    <div class="form-group text-center">
                        <a href="#" class="btn btn-primary" ng-click="filtered()">Фильтр</a>
                    </div>
                </th>
            </tr>
            </thead>
        </table>

        <?php ActiveForm::end(); ?>

        <div class="istream-schedule-wms-index">



            <h2 class="text-center">{{ vm.calendarTitle }}</h2>

            <div class="row">

                <div class="col-md-6 text-center">
                    <div class="btn-group">

                        <button
                            class="btn btn-primary"
                            mwl-date-modifier
                            date="vm.viewDate"
                            decrement="vm.calendarView">
                            Предыдущий
                        </button>
                        <button
                            class="btn btn-default"
                            mwl-date-modifier
                            date="vm.viewDate"
                            set-to-today>
                            Сегодня
                        </button>
                        <button
                            class="btn btn-primary"
                            mwl-date-modifier
                            date="vm.viewDate"
                            increment="vm.calendarView">
                            Следующий
                        </button>
                    </div>
                </div>

                <br class="visible-xs visible-sm">

                <div class="col-md-6 text-center">
                    <div class="btn-group">
                        <label class="btn btn-primary" ng-model="vm.calendarView" uib-btn-radio="'year'">Год</label>
                        <label class="btn btn-primary" ng-model="vm.calendarView" uib-btn-radio="'month'">Месяц</label>
                        <label class="btn btn-primary" ng-model="vm.calendarView" uib-btn-radio="'week'">Неделя</label>
                        <label class="btn btn-primary" ng-model="vm.calendarView" uib-btn-radio="'day'">День</label>
                    </div>
                </div>

            </div>

            <br>

            <mwl-calendar
                events="vm.events"
                view="vm.calendarView"
                view-title="vm.calendarTitle"
                view-date="vm.viewDate"
                cell-is-open="vm.isCellOpen"
                day-view-start="06:00"
                day-view-end="22:00"
                day-view-split="30"
                cell-modifier="vm.modifyCell(calendarCell)">
            </mwl-calendar>

        </div>
    </div>
        </p>
</div>