<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\models\User */
use kartik\editable\Editable;
use kartik\date\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

$model = new common\models\SCSPECIFICATIONPRICAT();
$company_id=Yii::$app->request->get('company_id');
?>


<div class="user-view internal-page">
    <h2 class="common-title">
        Спецификация
    </h2>
            <?php
            if(isset($company_ids)){ ?>
                <p>
                    <b>
                        Доступные поставщики:
                    </b>
                </p>
                <div class="list-group specifications-cmp">
                    <?php
                    foreach ($company_ids as $company){
                        echo '<a href="'.Url::toRoute(['site/specifications-list','company_id'=>$company['company_code']]).'" class="list-group-item list-group-item-action"> <i class="glyphicon glyphicon-folder-open"></i> '.$company['company_name'].'</a>';
                    } ?>
                </div>
            <?php }?>
    <div class="">

            <?php if(isset($data)){ ?>

                <?= Html::beginForm(['site/specifications-list?company_id='.$company_id.'&filter=1'], 'post', ['data-pjax' => '', 'class' => 'form-inline', 'id'=>'filterForm']); ?>
                <?= Html::input('text', 'string', Yii::$app->request->post('string'), ['class' => 'form-control','placeholder'=> 'Товар']) ?>
                <?= Html::submitButton('Фильтровать', ['class' => 'btn btn-lg btn-primary filter-send', 'name' => 'filter-button', 'id'=>'filter-submit']) ?>
                <a class="btn  btn-primary export-btn" id="infilter-form" href="<?php echo Url::current();?>&type=1"><i class="glyphicon glyphicon-save-file"></i> Экспорт(.csv)</a>
                <?= Html::endForm() ?>

                <?php Pjax::begin([
                    'id' =>'filterModel',
                    'enablePushState' => false,
                    'linkSelector' => 'btn.filter-send',
                    'timeout'=> 1000
                ]); ?>

                <table class="table table-striped table-bordered balance specification">
                    <thead class="thead-inverse">
                    <tr>
                        <th class="articul">Артикул</th>
                        <th>Товар</th>
                        <th>Цена</th>
                        <th class="text-center">Новая цена</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        $counter= 1;
                        foreach ($data as $d){
                            $ARTICUL = $d['Articul'];
                            $new_value = round(trim($d['Price_New']),2);
                            $new_value = (empty($new_value) ? '' : $new_value);
                            echo '<tr>';
                                echo '<td scope="row" class="articul">'.$ARTICUL.'</td>';
                                echo '<td>'.$d['Name'].'</td>';
                                echo '<td>'.round($d['Price'],2).'</td>';
                                echo '<td class="new-price text-center">';
                                    $editable = Editable::begin([
                                        'model'=>$model,
                                        'attribute'=>'Price_New',
                                        'header'=>'цену',
                                        'options'=>['id'=>$counter, 'placeholder'=>'Предложите цену', 'value'=>$d['Price_New']],
                                        'asPopover' => true,
                                        'placement'=> 'left',
                                        'inlineSettings' => [
                                            'templateBefore' => Editable::INLINE_BEFORE_2,
                                            'templateAfter' =>  Editable::INLINE_AFTER_2
                                        ],
                                        'displayValue' => '<i class="glyphicon glyphicon-edit"></i>'.$new_value,
                                        'ajaxSettings'=>[
                                            'url' => Url::to(Url::home().'site/save-price'),
                                        ],
                                        'showAjaxErrors' => true,
                                        'pluginEvents'=> [
                                            "editableSuccess"=>"function(event, val, form, data) { alert('Успешно предложена цена: ' + val); }",
                                            "editableAjaxError"=>"function(event, jqXHR, status, message) { alert(message); }",
                                        ],
                                    ]);
                                    $form = $editable->getForm();

                                    echo Html::hiddenInput('SCSPECIFICATIONPRICAT[Code_PRODUCT]', $ARTICUL);
                                    echo Html::hiddenInput('SCSPECIFICATIONPRICAT[ID_POSTAV]', $company_id);
                                    echo Html::hiddenInput('Price_New', $d['Price_New']);
                                    $editable->afterInput =
                                        $form->field($model, 'DateFrom')->widget(DatePicker::classname(), [
                                            'data'=>$data, // any list of values
                                            'options'=>['id'=> $counter,'placeholder'=>'Применить с:', 'value' => $d['DateFrom']],
                                            'pluginOptions'=>[
                                                'allowClear'=>true,
                                                'format' => 'yyyy-mm-dd',
                                                'todayHighlight' => true,
                                                'startDate'=> date('Y-m-d', strtotime('+7 days')),
                                            ]
                                        ])->label(false) . "\n";
                                    Editable::end();
                                echo '</td>';
                            echo '</tr>';
                            $counter++;
                           
                        }
                    ?>
                    </tbody>
                </table>

                <?php Pjax::end(); ?>

            <?php }?>
    </div>
    <p>
        Уважаемые партнеры, направляйте ваши коммерческие предложения по инвестициям и дополнительным условиям работы на электронную почту категорийному менеджеру компании "ТАВРИЯ В", который ведет вашу группу товаров.
    </p>
    <br/>
</div>