<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>
<div class="user-view internal-page">
    <h2 class="common-title">
        Сотрудничество с категорийным менеджером
    </h2>

    <p>
        <b>У торговой сети "ТАВРИЯ В" свыше 500 поставщиков.</b> Работа с ними строится по принципу обеспечения постоянного ассортимента, стабильного качества товаров и своевременности поставок.
    </p>
    <br/>
    <p>
        Кроме того, на поставщиков накладывается ряд технических требований, как, например, доставка товара исключительно в евроупаковке/европоддонах, наличие системы электронных накладных и прочее. Поставщик должен иметь достаточный финансовый потенциал для того, чтобы способствовать продвижению своей продукции с помощью рекламы и промо-акций. «ТАВРИЯ В» осуществляет централизованное управление закупками.    </p>
    <br/>
    <div class="files-box">
        <div class="anketa">
            <div class="icon-anketa">

                <a href="<?= Url::to('/files/application_for_provider.xls')?>">
                    <i class="anketa-img"></i>
                </a>
            </div>
            <div class="anketa-content">
                <h4>Анкета для поставщика</h4>
                <p>(.xls, 71 kb)</p>
                <p>
                    условия сотрудничества с <br>
                    компанией "ТАВРИЯ В"
                </p>
                <p>
                    <a href="<?= Url::to('/files/application_for_provider.xls')?>">Скачать</a>
                </p>
            </div>
        </div>
        <div class="anketa">
            <div class="icon-anketa">
                <?= Html::a('',Yii::$app->homeUrl);?>
                <a href="<?= Url::to('/files/form_for_receive_logistic_data.xls')?>">
                    <i class="anketa-img"></i>
                </a>
            </div>
            <div class="anketa-content">
                <h4>
                    Форма для передачи <br>
                    логистических данных
                </h4>
                <p>(.xls, 822 kb)</p><br/>
                <p>
                    <a href="<?= Url::to('/files/form_for_receive_logistic_data.xls')?>">Скачать</a>
                </p>
            </div>
        </div>
    </div>

    <div class="require-list">
        <h4>Как стать поставщиком или ввести новый товар в ассортимент "ТАВРИЯ В":</h4>
        <ul>
            <li>Получите информацию об электронной анкете-заявке у офис-менеджера «ТАВРИЯ В», либо по телефону +380 (482) 307-305, либо на корпоративном сайте www.tavriav.org.</li>
            <li>Заполните электронную анкету-заявку и отправьте ее по адресу: price@tavriav.com.ua.</li>
            <li>Распечатайте анкету-заявку, заверьте ее подписью руководителя организации и «мокрой» печатью.</li>
            <li>Предоставьте анкету-заявку вместе с образцами товара и документами, удостоверяющими качество товара (сертификаты, свидетельства и т. п.) офис-менеджеру «ТАВРИЯ В».</li>
            <li>Образцы товара принимаются только при условии наличия правильно заполненной анкеты-заявки, а также всех документов, удостоверяющих качество товара.</li>
            <li>Совет директоров рассматривает предложения о введении нового поставщика или нового товара каждый четверг.</li>
            <li>Менеджер категории в электронном виде проинформирует вас о решении, принятом советом директоров.</li>
        </ul>

    </div>
    <div class="contact-form">
        <h4>"ТАВРИЯ В" за эффективное сотрудничество!</h4>
        <div class="form-box-cream">
            <h3>Отправить анкету</h3>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($model, 'name')->textInput(['autofocus' => true])->label('Ф. И. О.') ?>

            <?= $form->field($model, 'email')->label('Электронная почта') ?>

            <?= $form->field($model, 'company')->label('Компания') ?>

            <?= $form->field($model, 'position')->label('Должность') ?>

            <?= $form->field($model, 'message')->textarea(['rows' => 6])->label('Дополнительная информация') ?>

            <?= $form->field($model, 'phone')->textInput(['autofocus' => true])->label('Телефон') ?>
            
            <?= $form->field($model, 'files')->fileInput()->label('Анкета') ?>

            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-send', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>