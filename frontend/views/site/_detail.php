<table class="table table-striped table-bordered detail-viewer">
    <thead>
    <tr>
        <th>Номер</th>
        <th>Склад</th>
        <th>Номер заказа</th>
        <th>Дата</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?php if(isset($number)){ echo $number; }?></td>
        <td><?php if(isset($store)){ echo $store; }?></td>
        <td><?php if(isset($numb_ord)){ echo $numb_ord; }?></td>
        <td><?php if(isset($date)){ echo $date; }?></td>
    </tr>
    </tbody>
</table>
</div>
<div class="modal-body table-responsive">
    <?php if(isset($detailPart)){?>
    <table class="table table-striped table-bordered balance">
        <thead>
            <th>№</th>
            <th>Продукт</th>
            <th>№ Покупателя</th>
            <th>Доставлено</th>
            <th>Кол-во</th>
            <th>Цена (грн.)</th>
        </thead>
        <tbody>

            <?php
            $sum_price = 0;
            foreach ($detailPart as $v){
                echo '<tr>';
                    echo '<td>'.$v['POSITIONNUMBER'].'</td>';
                    echo '<td>'.$v['PRODUCT'].'</td>';
                    echo '<td>'.$v['PRODUCTIDBUYER'].'</td>';
                    echo '<td>'.$v['DELIVEREDQUANTITY'].'</td>';
                    echo '<td>'.$v['ORDEREDQUANTITY'].'</td>';
                    echo '<td>'.$v['PRICE'].'</td>';
                echo '</tr>';
                $sum_price += $v['PRICE'];
            }
            ?>
            <tr>
                <td></td> 
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Сумма: <?= $sum_price;?></td>
            </tr>


        </tbody>
    </table>
        <?php
    }
    ?>
</div>
