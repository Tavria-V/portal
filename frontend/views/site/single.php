<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\User */
 
?>
<div class="user-view internal-page">
    <?php if(isset($article)){ ?>
        <?php foreach ($article as $post) { ?>
            <h2 class="common-title">
                <?php  echo $post['title'];?>
            </h2>
        <p>
            <?php  echo $post['content'];?>
        </p>

        <?php }?>


    <?php }?>
</div>