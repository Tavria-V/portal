<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use himiklab\thumbnail\EasyThumbnailImage;
?>

<h1 class="main-title">Портал поставщика</h1>
<?php echo Html::img('@web/images/stamp.png',['alt'=>'Портал поставщика', 'class'=>'stamp-mark'])?>
<p class="sub-title">
    <b>У торговой сети "ТАВРИЯ В" свыше 500 поставщиков.</b>
</p>
<p>
    Работа с ними строится по принципу обеспечения постоянного ассортимента, стабильного качества товаров и своевременности поставок.
</p>
<p>
    Кроме того, на поставщиков накладывается ряд технических требований, например, доставка товара исключительно в евроупаковке/европоддонах, наличие системы электронных накладных и пр.
</p>
<div class="news-box">
    <h2 class="common-title">
        Новости
    </h2>
    <?php if (isset($NEWS)){ ?>
    <?php foreach ($NEWS as $article){

        ?>
        <div class="news-item">
            <a href="<?=  'site/news?id='.$article['id']?>">
                <?php

                echo EasyThumbnailImage::thumbnailImg(
                    '@frontend/web/uploads/'.$article['image'],
                    228,
                    160,
                    EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                    ['alt' => $article['title']]
                );

                ?>

                <div class="description-news">


                    <p class="news-date"> <?= Yii::$app->Formatter->asDate($article['create_date'], 'php:d F') ?> </p>
                    <h3 class="news-title one-height"><?= $article['title']?></h3>
                </div>
            </a>
        </div>
    <?php } ?>
    <?php }?>
</div>

<p class="bottom-text">
    Поставщик должен иметь достаточный финансовый потенциал для того, чтобы способствовать продвижению своей продукции с помощью рекламы и промоакций. "ТАВРИЯ В" осуществляет централизованное управление закупками.
</p>
