<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model frontend\models\User */

$company_id=Yii::$app->request->get('company_id');
?>
<div class="user-view internal-page">
    <h2 class="common-title">
         Остатки 
    </h2>
    <?php
    if(isset($company_ids)){ ?>
        <p>
            <b>
                Доступные поставщики:
            </b>
        </p>
        <div class="list-group specifications-cmp">
            <?php
            foreach ($company_ids as $company){
                echo '<a href="'.Url::to(Url::home().'site/get-product-list?company_id='.$company['company_code']).'" class="list-group-item list-group-item-action"> <i class="glyphicon glyphicon-folder-open"></i> '.$company['company_name'].'</a>';
            } ?>
        </div>
    <?php }?>
    <div class="">
        <?php Pjax::begin([
            'id' =>'filterModel',
            'enablePushState' => false,
            'linkSelector' => 'btn.filter-send',
            'timeout'=> 1000
        ]); ?>
            <?php if(isset($data)){ ?>
                <div class="text-center">
                    <?= Html::beginForm(['site/get-product-list?company_id='.$company_id.'&filter=1'], 'post', ['data-pjax' => '', 'class' => 'form-inline', 'id'=>'filterForm']); ?>
                    <?= Html::input('text', 'string', Yii::$app->request->post('string'), ['class' => 'form-control','placeholder'=> 'Наименование']) ?>
                    <?= Html::submitButton('Фильтровать', ['class' => 'btn btn-lg btn-primary filter-send', 'name' => 'filter-button']) ?>

                    <a class="btn  btn-primary export-btn" id="infilter-form" href="<?php echo Url::current();?>&type=1"><i class="glyphicon glyphicon-save-file"></i> Экспорт(.csv)</a>
                    <?= Html::endForm() ?>
                </div>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'emptyText' => '-',
                    'options' => [
                        'class' => ''
                    ],
                    'columns' => [
                        [
                            'attribute' => 'Articul',
                            'format' => 'text',
                            'label' => 'Артикул',
                            'contentOptions' => [
                                'class' => 'articul'
                            ],
                            'headerOptions' => [
                                'class' => 'articul'
                            ],
                        ],
                        [
                            'attribute' => 'Name',
                            'format' => 'text',
                            'label' => 'Товар',
                        ],
                        [
                            'attribute' => 'Store_name',
                            'format' => 'text',
                            'label' => 'Склад',
                            'contentOptions' => [
                                'class' => 'store'
                            ],
                            'headerOptions' => [
                                'class' => 'store'
                            ],

                        ],
                        [
                            'attribute' => 'Qtty',
                            'format' => ['decimal',2],
                            'label' => 'Кол-во'
                        ],
                        [
                            'attribute' => 'Price',
                            'format' => ['decimal',2],
                            'label' => 'Цена'
                        ],
                    ],
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered balance'
                    ]
                ]) ?>
            <?php }?>
        <?php Pjax::end(); ?>

    </div>
    <p>
        Уважаемые партнеры, направляйте ваши коммерческие предложения по инвестициям и дополнительным условиям работы на электронную почту категорийному менеджеру компании "ТАВРИЯ В", который ведет вашу группу товаров.
    </p>
    <br/>
</div>