<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\FontAsset;
use frontend\assets\CalendarAsset;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

AppAsset::register($this);
FontAsset::register($this);
CalendarAsset::register($this);
voskobovich\matchheight\MatchHeightAsset::register($this);
//
$this->registerJs('$(".one-height").matchHeight();');

$model=new \backend\models\LoginForm();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="mwl.calendar.docs">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/interact.js/1.2.4/interact.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.js"></script>
    <script src="https://code.angularjs.org/1.0.8/i18n/angular-locale_ru-ru.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.14.3/ui-bootstrap-tpls.min.js"></script>
    <script src="//mattlewis92.github.io/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js"></script>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="//mattlewis92.github.io/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title>Портал поставщика. Таврия В.</title>
    <?php $this->head() ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-43431469-2', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

        <div class="container main-wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-xs-12 col-sm-12 left-sidebar header-part">
                    <?= Html::a(Html::img('@web/images/logo.jpg', ['alt'=>Yii::$app->name, 'class'=>'logotype']), Yii::$app->homeUrl)?>
                </div>
                <div class="col-lg-9 col-md-8 col-xs-12 col-sm-12 text-right header-row border-right border-left">
                    <ul class="language-box">
                        <li class="current">
                            <a>RU</a>
                        </li>
                        <li>
                            <a>UA</a>
                        </li>
                    </ul>

                    <a href="tel:+380 (482) 307-305" class="header-call-us">
                        <i class="glyphicon glyphicon-earphone"></i>
                        +380 (482) 307-305
                    </a>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4 col-xs-12 col-sm-12 left-sidebar">
                    <?php
                    $leftMenu = [
//                        ['label' => 'Организация встречи с менеджером', 'url' => ['/site/meeting-manager']],
                        ['label' => 'Сотрудничество с категорийным менеджером', 'url' => ['/site/cooperation']],
                        ['label' => 'Обращение к руководству', 'url' => ['/site/appeal']],
                    ];
                    ?>
                    <?php if (!Yii::$app->user->isGuest) {?>
                        <div class="main-menu">
                            <?php
                            $leftMenu = [
                                ['label' => 'Профиль', 'url' => ['/user/view?id='.Yii::$app->user->id]],
                                ['label' => 'Продажи', 'url' => ['/site/balances']],
                                ['label' => 'Остатки', 'url' => ['/site/list-products']],
                                ['label' => 'Спецификация', 'url' => ['/site/specifications']],
                                ['label' => 'Календарь поставок', 'url' => ['/site/calendar']],
                                ['label' => 'Сотрудничество с категорийным менеджером', 'url' => ['/site/cooperation']],
                                ['label' => 'Обращение к руководству', 'url' => ['/site/appeal']],
                                ['label' => 'Отгрузка товаров', 'url' => ['/site/delivery-status']], 
                                ['label' => 'Журнал заказов', 'url' => ['/site/orders-journal']],
                            ];
                            $leftMenu[] = '<li>'
                                . Html::beginForm(['/site/logout'], 'post')
                                . Html::submitButton(
                                    'Выход (' . Yii::$app->user->identity->fio . ')',
                                    ['class' => 'btn btn-link']
                                )
                                . Html::endForm()
                                . '</li>';
                            ?>
                        </div>
                    <?php } ?>
                                        <?php
                                        NavBar::begin([
                                            'brandLabel' => 'Портал поставщика',
                                        ]);

                                        echo Nav::widget([
                                            'options' => ['class' => 'left-menu','id'=>'w1'],
                                            'items' => $leftMenu,
                                        ]);
                                        NavBar::end();
                                        ?>

                    <div class="login-form mobile-only">
                        <?php if (Yii::$app->user->isGuest) {?>
                            <h3 class="entry-title">Вход</h3>
                            <?php $form = ActiveForm::begin([
                                'id' => 'login-form',
                                'action' => Url::to(Url::home().'site/login'),
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => false,
                                'validationUrl' => Url::to(Url::home().'site/validation-login'),
                                'validateOnSubmit'=> true,
                                'class' => 'form-horizontal',
                            ]);
                            ?>
                            <?= $form->field($model, 'email')->textInput(['placeholder'=>'Email'])->label(false) ?>
                            <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Пароль'])->label(false)?>


                            <?= $form->field($model, 'rememberMe')->checkbox(['class'=>'remember-link'])->label('Запомнить меня') ?>

                            <div style="color:#999;margin:1em 0">
                                <?= Html::a('Забыли пароль?', ['request-password-reset'], ['class'=>'forgot-btn']) ?>
                            </div>

                            <div class="form-group">
                                <?= Html::submitButton('Войти', ['class' => 'btn btn-login', 'name' => 'login-button']) ?>
                            </div>
                            <?php $form->end();?>

                        <?php } ?>
                        <div class="help-line desktop-only">
                            <a href="tel:0 800 50 00 40">
                                <?php echo Html::img('@web/images/red-phone.png', ['alt'=>Yii::$app->name, 'class'=>'red-phone'])?>
                                0 800 50 00 40 <br>
                                <span>Линия помощи</span>
                            </a>

                        </div>
                    </div>

                </div>
                <div class="col-lg-9 col-md-8 col-xs-12 col-sm-12 content border-top ">


                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= Alert::widget() ?>
                    <?= $content ?>

                    <div class="help-line mobile-only">
                        <a href="tel:0 800 50 00 40">
                            <?php echo Html::img('@web/images/red-phone.png', ['alt'=>Yii::$app->name, 'class'=>'red-phone'])?>
                            0 800 50 00 40 <br>
                            <span>Линия помощи</span>
                        </a>

                    </div>

                </div>
            </div>

        </div>
</div>
    <footer class="footer">
        <div class="container">
            <p>&copy; 2012 - <?= date('Y') ?>. СЕТЬ СУПЕРМАРКЕТОВ "ТАВРИЯ В".</p>
        </div>
    </footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
