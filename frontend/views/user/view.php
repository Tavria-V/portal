<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
    <?php
    $cmp = '';
    if(isset($company)){
        foreach ($company as $v){
            $cmp .= $v['company_name'].'; ';
        }
    }
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                        'id',
            'fio',
            [
                'attribute'=>'status',
                'value'=>$model->status=='10' ?
                    'Активный' :
                    'Не активный',
                'displayOnly'=>true,
            ],
            'department',

            'phone',
            [
                'attribute'=>'position',
                'value'=> $model->position=='manager' ? 'Менеджер': 'Мерчендайзер',
            ],
            [
                'attribute'=>'company',
                'value'=>$cmp,
            ],
        ],
    ]) ?>

</div>
