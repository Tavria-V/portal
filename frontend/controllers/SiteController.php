<?php
namespace frontend\controllers;

use common\components\CustomCsv;
use common\models\News;
use common\models\SCSPECIFICATIONPRICAT;
use common\models\UserCompanies;
use frontend\models\AppealForm;
use frontend\models\CooperateForm;
use common\models\SALESDETAILCHILD;
use common\models\SCPRODUCT;
use common\models\REMAINDERCUR;
use common\models\IStreamClient;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseJson;
use yii\helpers\Json;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile; 
use app\models\IStreamScheduleSearch;
use app\models\IStreamSchedule;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $NEWS = (new \yii\db\Query())
            ->select(['id', 'title', 'content', 'image', 'create_date', 'priority'])
            ->from('news')
            ->where(['status' => 1])
            ->all();
        $ArrayProvider = new ArrayDataProvider([
            'allModels'=>$NEWS,
            'pagination'=> [
                'pageSize'=> 9,
            ]
        ]);
        return $this->render('index', ['NEWS' => $NEWS, 'dataProvider'=> $ArrayProvider]);
    }

    public function actionNews($id){
        $article = (new \yii\db\Query())
            ->select(['id', 'title', 'content', 'image', 'create_date', 'priority'])
            ->from('news')
            ->where(['status' => 1])->andWhere(['id'=> $id])
            ->all();
        return $this->render('single', ['article'=> $article]);
    }

    public function actionCalendar(){
        $searchModel = new IStreamScheduleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('calendar',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /*************
     * Controller AJAX Auto complete
     ************/
    public function actionClientList($q = null, $id = null) {

        $CURRENT_USER_ID = Yii::$app->user->getId();
        $USER_COMPANIES = UserCompanies::find()
            ->with(['clients','stores'])
            ->select(['CODE'])
            ->where(['user_id'=>$CURRENT_USER_ID])
            ->asArray(true)
            ->all();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
         //   $q=trim(iconv("utf-8", "windows-1251//TRANSLIT//IGNORE", $q));
            $client =  IStreamClient::find()
                ->select(['ID_CLIENT','DESCR'])
                ->where(['like','DESCR',$q])
                ->andWhere(['IN','CODE', ArrayHelper::map($USER_COMPANIES,'CODE','CODE')])
                ->all();
            $out=[];
            foreach ($client as $c)
            {
                $out['results'][]=['id'=>$c->ID_CLIENT,'text'=>$c->DESCR];
            }
        }
        return $out;
    }

    public function GetUserCompanies(){
        $CURRENT_USER_ID = Yii::$app->user->getId();
        $USER_COMPANIES = UserCompanies::find()
            ->with(['clients','stores'])
            ->select(['CODE'])
            ->where(['user_id'=>$CURRENT_USER_ID])
            ->asArray(true)
            ->all();
        return $USER_COMPANIES;
    }

    public function actionGetData()
    {
        
        $data= IStreamSchedule::find()
            ->with(['clients','stores'])
            ->where(['IN','Code_client', ArrayHelper::map($this->GetUserCompanies(),'CODE','CODE')])
            ->all();
        $events = array();
        $dataResult = array();
        function get_status($status){
            if ($status == 0 || is_null($status)){
                return 'important';
            }else{
                return 'info';
            }
        }
        foreach ($data as $a)
        {
            $originalDate = $a->DateOrder;
            $originalDate2 = $a->DateDelivery;

            $startDate = date("Y-m-d H:i:s", strtotime($originalDate));
            $endDate = date("Y-m-d H:i:s", strtotime($originalDate2));

            $events['ID']= $a->ID;
            $events['title'] = trim($a->clients->DESCR).'('.trim($a->clients->FIO).')'.' &rarr; '.trim($a->stores->DESCR);
            $events['startsAt'] = $startDate;
            $events['endsAt'] = $endDate;
            $events['type'] = get_status($a->Status);
            $events['draggable'] = false;
            $events['resizable'] = false;

            $dataResult[]=$events;
        }

        $EVENTS = Json::encode($dataResult);

        return $EVENTS;
    }

    public function actionFilterCheck(){

        $request = Yii::$app->request;

        $Filter = $request->get();

        if(!empty($Filter['IStreamScheduleSearch']['Code_client'])){
            $searchModel= new IStreamScheduleSearch();
            $searchModelresult=$searchModel->search($request->get());

            $data = $searchModelresult->getModels();

            $events = array();
            $dataResult = array();
            function get_status($status){
                if ($status == 0 || is_null($status)){
                    return 'important';
                }else{
                    return 'info';
                }
            }
            foreach ($data as $a=>$value)
            {
                $originalDate = $value->DateOrder;
                $originalDate2 = $value->DateDelivery;

                $startDate = date("Y-m-d H:i:s", strtotime($originalDate));
                $endDate = date("Y-m-d H:i:s", strtotime($originalDate2));

                $events['ID']= $value->ID;
                $events['title'] = trim($value->clients->DESCR).'('.trim($value->clients->FIO).')'.' &rarr; '.trim($value->stores->DESCR);
                $events['startsAt'] = $startDate;
                $events['endsAt'] = $endDate;
                $events['type'] = get_status($value->Status);
                $events['draggable'] = false;
                $events['resizable'] = false;

                $dataResult[]=$events;
            }

            $EVENTS = Json::encode($dataResult);
            return $EVENTS;
        }else{
           return $this->actionGetData();
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionValidationLogin()
    {
        $model = new LoginForm();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }



    public function actionProfile()
    {
        return $this->render('update');
    }


    public function actionAppeal(){
        return $this->render('appeal');
    }
    public function actionMeetingManager(){
        return $this->render('meeting');
    }

    public function GetCompaniesIds(){
        $user_cmp = new UserCompanies();
        $company_ids = $user_cmp->find()->where(['user_id'=> Yii::$app->user->getId()])->asArray()->all();
        return $company_ids;
    }

    public function actionDeliveryStatus(){
        $companies = $this->GetCompaniesIds();
        return $this->render('delivery', ['company_ids'=>$companies]);
    }

    public function actionGetDesadvList($company_code){
        $check = $this->UserChekerCode($company_code);

        if($check){
            $start_date = date('Y-m-d', strtotime('-7 days'));
            $Document = (new \yii\db\Query())
                ->select([
                    'istream_Desadv.FileName',
                    'istream_Desadv.NUMBER',
                    'istream_Desadv.DATE',
                    'iStreamStore.DESCR',
                    'istream_Desadv.DELIVERYDATE', //4
                    'istream_Desadv.ORDERNUMBER', //3
                    'istream_Desadv.ORDERDATE',
                    'istream_Desadv.DELIVERYNOTENUMBER', //1 *
                    'istream_Desadv.DELIVERYPLACE', //2 *
                    'istream_Desadv.CREATED' //5
                ])
                ->from('istream_Desadv')
                ->leftJoin('iStream_Orders', 'iStream_Orders.FileName=istream_Desadv.ORDERNUMBER')
                ->leftJoin('iStreamStore', 'iStreamStore.GLN=istream_Desadv.DELIVERYPLACE')
                ->where(['iStream_Orders.Code_postav'=>$company_code])
                ->andWhere(['between', 'istream_Desadv.DELIVERYDATE', $start_date, date('Y-m-d')])
                ->orderBy(['DELIVERYDATE'=>'ASC', 'CREATED'=>'ASC'])
                ->all(\Yii::$app->olap);

            $companies = $this->GetCompaniesIds();
            return $this->render('delivery', ['company_ids'=>$companies,'document'=>$Document]);
        }else{
            return $this->render('error_permissions');
        }

    }

    public function actionGetDesadevDetailView($filename,$number,$store,$numb_ord,$date){
            $detailView = (new \yii\db\Query())
                ->select(['POSITIONNUMBER', 'PRODUCT', 'PRODUCTIDBUYER', 'DELIVEREDQUANTITY', 'ORDEREDQUANTITY', 'PRICE'])
                ->from('iStream_Desadv_Detail')
                ->where(['filename' => $filename])
                ->all(\Yii::$app->olap);
            return $this->renderAjax('_detail', ['detailPart'=>$detailView, 'number'=>$number, 'store'=>$store, 'numb_ord'=>$numb_ord,'date'=>$date]);
    }

    public function actionOrdersJournal(){
        $companies = $this->GetCompaniesIds();
        return $this->render('journal', ['company_ids'=> $companies]);
    }

    public function actionGetOrdersInfo($company_code){
        $check = $this->UserChekerCode($company_code);

        if($check){
            $start_date = date('Y-m-d', strtotime('-7 days'));
            $document = (new \yii\db\Query())
                ->select([
                    'iStream_Orders.FileName',
                    'iStream_Orders.DATE',
                    'iStream_Orders.DELIVERYDATE',
                    'iStream_Orders.DELIVERYPLACE',
                    'iStreamStore.DESCR',
                    'iStream_Orders.CREATED',
                    'iStream_Orders.UPDATED',
                    'iStream_ORDRSP.NUMBER',
                    'iStream_ORDRSP.DATE',
                    'iStream_ORDRSP.TIME',
                    'iStream_ORDRSP.ORDERDATE',
                    'iStream_ORDRSP.ACTION',
                ])
                ->from('iStream_Orders')
                ->leftJoin('iStream_ORDRSP', 'iStream_Orders.FileName=iStream_ORDRSP.ORDERNUMBER')
                ->leftJoin('iStreamStore', 'iStreamStore.GLN=iStream_Orders.DELIVERYPLACE')
                ->where(['iStream_Orders.Code_postav'=>$company_code])
                ->andWhere(['between', 'iStream_Orders.date', $start_date, date('Y-m-d')])
                ->all(\Yii::$app->olap);
            return $this->render('journal', ['document'=>$document]);
        }else{
            return $this->render('error_permissions');
        }

    }

    public function actionCooperation(){

        $model = new CooperateForm();

        if ($model->load(Yii::$app->request->post())){
            $model->files = UploadedFile::getInstance($model, 'files');
            $model->upload();
            return $this->refresh();
        } else {
            return $this->render('cooperation', [
                'model' => $model,
            ]);
        }

    }


    public function actionBalances(){
        $user_cmp = new UserCompanies();
        $company_ids = $user_cmp->find()->where(['user_id'=> Yii::$app->user->getId()])->asArray()->all();
        return $this->render('balances', ['company_ids'=> $company_ids]);

    }

    public function UserCheker($cmp_id){
        $user_cmp = new UserCompanies();

        $result = $user_cmp->find()
            ->where(['user_id'=> Yii::$app->user->getId()])
            ->andWhere(['company_code' =>$cmp_id])
            ->all();
        if($result){
            return 1;
        }else{
            return 0;
        }
    }
    public function UserChekerCode($cmp_code){
        $user_cmp = new UserCompanies();

        $result = $user_cmp->find()
            ->where(['user_id'=> Yii::$app->user->getId()])
            ->andWhere(['CODE' =>$cmp_code])
            ->all();
        if($result){
            return 1;
        }else{
            return 0;
        }
    }

    public function actionGetBalance($company_id, $type = 0, $filter = 0){
        $check = $this->UserCheker($company_id);

        if($check){

            $connection = Yii::$app->OlapSrv;

            if($filter == true && !empty(Yii::$app->request->post('string'))) {
                $product = Yii::$app->request->post('string');
                //  $product = trim(iconv("UTF-8", "windows-1251//TRANSLIT//IGNORE", $product));
                $command = $connection->createCommand("
        SELECT SC_PRODUCT.CODE as Articul,
        SC_PRODUCT.DESCR as Name,
        sc_store.DESCR as Store_name,
        SALES_DETAIL_CHILD.DOC_DATE as date,
        sum(SALES_DETAIL_CHILD.QUANTITY) as Qtty,
        Sum(SALES_DETAIL_CHILD.SUM_PROD) as Sum
        FROM SALES_DETAIL_CHILD 
        left join SC_PRODUCT on SC_PRODUCT.ID_PRODUCT=SALES_DETAIL_CHILD.ID_PRODUCT
        left join SC_STORE on SC_STORE.ID_STORE=SALES_DETAIL_CHILD.ID_STORE
        where SALES_DETAIL_CHILD.ID_COMPANY=:param
        and SC_PRODUCT.DESCR LIKE :product
        group by SC_PRODUCT.CODE,SC_PRODUCT.DESCR,sc_store.DESCR,SALES_DETAIL_CHILD.DOC_DATE
        Order by SALES_DETAIL_CHILD.DOC_DATE desc, sc_store.DESCR/*,2,3,4*/
        ",[':param'=>$company_id, ':product'=>'%'.$product.'%']);
                $data = $command->queryAll();
            }else{

                $command = $connection->createCommand("
        SELECT SC_PRODUCT.CODE as Articul,
        SC_PRODUCT.DESCR as Name,
        sc_store.DESCR as Store_name,
        SALES_DETAIL_CHILD.DOC_DATE as date,
        sum(SALES_DETAIL_CHILD.QUANTITY) as Qtty,
        Sum(SALES_DETAIL_CHILD.SUM_PROD) as Sum
        FROM SALES_DETAIL_CHILD 
        left join SC_PRODUCT on SC_PRODUCT.ID_PRODUCT=SALES_DETAIL_CHILD.ID_PRODUCT
        left join SC_STORE on SC_STORE.ID_STORE=SALES_DETAIL_CHILD.ID_STORE
        where SALES_DETAIL_CHILD.ID_COMPANY=:param
        group by SC_PRODUCT.CODE,SC_PRODUCT.DESCR,sc_store.DESCR,SALES_DETAIL_CHILD.DOC_DATE
        Order by SALES_DETAIL_CHILD.DOC_DATE desc, sc_store.DESCR/*,2,3,4*/
        ",[':param'=>$company_id]);
                $data = $command->queryAll();
            }

            $ArrayProvider = new ArrayDataProvider([
                'allModels'=> $data,
                'pagination'=> [
                    'pageSize'=> 50,
                ]
            ]);


            if($type==0){
                return $this->render('balances', ['data'=>$data, 'dataProvider'=>$ArrayProvider]);
            }else{
                return CustomCsv::widget([
                    'data'=>$data,
                ]);
            }
        }else{
            return $this->render('error_permissions');
        }

    }

    public function actionListProducts(){
        $user_cmp = new UserCompanies();
        $company_ids = $user_cmp->find()->where(['user_id'=> Yii::$app->user->getId()])->asArray()->all();
        return $this->render('list', ['company_ids'=> $company_ids]);

    }
    public function actionGetProductList($company_id, $type=0, $filter=0){
        $check = $this->UserCheker($company_id);

        if($check){
            $connection = Yii::$app->OlapSrv;
            if($filter == true && !empty(Yii::$app->request->post('string'))) {
                $product = Yii::$app->request->post('string');
                //   $product = trim(iconv("UTF-8", "windows-1251//TRANSLIT//IGNORE", $product));
                $command = $connection->createCommand("
        SELECT SC_PRODUCT.CODE as Articul
        ,SC_PRODUCT.DESCR as Name
        ,SC_STORE.DESCR as Store_name
        ,SC_STORE.Code as Store_code
        ,REMAINDER_CUR.QUANTITY as Qtty
        ,REMAINDER_CUR.SUM_GRN as Price
        FROM skladolap.dbo.REMAINDER_CUR 
        left join SC_STORE on SC_STORE.ID_STORE=REMAINDER_CUR.ID_STORE
        left join SC_PRODUCT on SC_PRODUCT.ID_PRODUCT=REMAINDER_CUR.ID_PRODUCT
        where REMAINDER_CUR.ID_COMPANY=:param
        and SC_PRODUCT.DESCR LIKE :product
        ORDER by SC_STORE.DESCR
        ",[':param'=>$company_id, ':product'=>'%'.$product.'%']);
                $data = $command->queryAll();
            }else{
                $command = $connection->createCommand("
        SELECT SC_PRODUCT.CODE as Articul
        ,SC_PRODUCT.DESCR as Name
        ,SC_STORE.DESCR as Store_name
        ,SC_STORE.Code as Store_code
        ,REMAINDER_CUR.QUANTITY as Qtty
        ,REMAINDER_CUR.SUM_GRN as Price
        FROM skladolap.dbo.REMAINDER_CUR 
        left join SC_STORE on SC_STORE.ID_STORE=REMAINDER_CUR.ID_STORE
        left join SC_PRODUCT on SC_PRODUCT.ID_PRODUCT=REMAINDER_CUR.ID_PRODUCT
        where REMAINDER_CUR.ID_COMPANY=:param
        ORDER by SC_STORE.DESCR
        ",[':param'=>$company_id]);
                $data = $command->queryAll();
            }
            $ArrayProvider = new ArrayDataProvider([
                'allModels'=> $data,
                'pagination'=> [
                    'pageSize'=> 50,
                ]
            ]);
            if($type==0){
                return $this->render('list', ['data'=>$data, 'dataProvider'=>$ArrayProvider]);
            }else {
                return CustomCsv::widget([
                    'data' => $data,
                ]);
            }
        }else{
            return $this->render('error_permissions');
        }
    }

    public function actionSpecifications(){
        $user_cmp = new UserCompanies();
        $company_ids = $user_cmp->find()->where(['user_id'=> Yii::$app->user->getId()])->asArray()->all();
        return $this->render('specifications', ['company_ids'=> $company_ids]);

    }
    public function actionSpecificationsList($company_id, $type=0, $filter=0){
        $check = $this->UserCheker($company_id);

        if($check){
            $connection = Yii::$app->OlapCost;

            if($filter == true && !empty(Yii::$app->request->post('string'))){
                $product = Yii::$app->request->post('string');
                //      $product=trim(iconv("UTF-8", "windows-1251//TRANSLIT//IGNORE", $product));
                $command = $connection->createCommand("
            SELECT SC_CLIENT.Id_CLIENT,
            SC_PRODUCT.Code_PRODUCT as Articul,
            SC_PRODUCT.DESCR as Name,
            SC_SPECIFICATION.Cost as Price,
            (SELECT TOP 1 Price_New FROM SC_SPECIFICATION_PRICAT WHERE SC_SPECIFICATION_PRICAT.Code_PRODUCT=SC_PRODUCT.Code_PRODUCT and SC_CLIENT.Id_CLIENT=SC_SPECIFICATION_PRICAT.ID_POSTAV ORDER BY Created DESC )  as Price_New,
            (SELECT TOP 1 DateFrom FROM SC_SPECIFICATION_PRICAT WHERE SC_SPECIFICATION_PRICAT.Code_PRODUCT=SC_PRODUCT.Code_PRODUCT and SC_CLIENT.Id_CLIENT=SC_SPECIFICATION_PRICAT.ID_POSTAV ORDER BY Created DESC ) as DateFrom
            FROM SC_SPECIFICATION with (nolock), SC_PRODUCT with (nolock) , SC_CLIENT with (nolock)
            where SC_SPECIFICATION.ID_PRODUCT= SC_PRODUCT.ID_PRODUCT 
            and SC_CLIENT.Id_CLIENT=SC_SPECIFICATION.ID_POSTAV
            and SC_SPECIFICATION.ID_TradeMode='86' 
            and SC_SPECIFICATION.Cost>'0'
            and SC_CLIENT.Id_CLIENT=:param
            and SC_PRODUCT.DESCR LIKE :product
            order by SC_SPECIFICATION.Cost desc
            ",[':param'=> $company_id, ':product'=>'%'.$product.'%']);
                $data = $command->queryAll();
            }else{
                $command = $connection->createCommand("
            SELECT SC_CLIENT.Id_CLIENT,
            SC_PRODUCT.Code_PRODUCT as Articul,
            SC_PRODUCT.DESCR as Name,
            SC_SPECIFICATION.Cost as Price,
            (SELECT TOP 1 Price_New FROM SC_SPECIFICATION_PRICAT WHERE SC_SPECIFICATION_PRICAT.Code_PRODUCT=SC_PRODUCT.Code_PRODUCT and SC_CLIENT.Id_CLIENT=SC_SPECIFICATION_PRICAT.ID_POSTAV ORDER BY Created DESC )  as Price_New,
            (SELECT TOP 1 DateFrom FROM SC_SPECIFICATION_PRICAT WHERE SC_SPECIFICATION_PRICAT.Code_PRODUCT=SC_PRODUCT.Code_PRODUCT and SC_CLIENT.Id_CLIENT=SC_SPECIFICATION_PRICAT.ID_POSTAV ORDER BY Created DESC ) as DateFrom
            FROM SC_SPECIFICATION with (nolock), SC_PRODUCT with (nolock) , SC_CLIENT with (nolock)
            where SC_SPECIFICATION.ID_PRODUCT= SC_PRODUCT.ID_PRODUCT and SC_CLIENT.Id_CLIENT=SC_SPECIFICATION.ID_POSTAV
            and SC_SPECIFICATION.ID_TradeMode='86' and SC_SPECIFICATION.Cost>'0'
            and SC_CLIENT.Id_CLIENT=:param
            order by SC_SPECIFICATION.Cost desc
            ",[':param'=> $company_id]);
                $data = $command->queryAll();
            }
            $ArrayProvider = new ArrayDataProvider([
                'allModels'=> $data,
                'pagination'=> [
                    'pageSize'=> 50,
                ]
            ]);
            if($type==0){
                return $this->render('specifications', ['data'=>$data, 'dataProvider'=>$ArrayProvider]);
            }else {
                return CustomCsv::widget([
                    'data' => $data,
                ]);
            }
        }else{
            return $this->render('error_permissions');
        }
    }

    public function actionSavePrice(){

        $model = new SCSPECIFICATIONPRICAT(); // your model can be loaded here
        $request=\Yii::$app->request;

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        // Check if there is an Editable ajax request
        if (isset($_POST['hasEditable'])) {
            // use Yii's response format to encode output as JSON
              $created = date('Y-m-d H:i:s');
            $model->Created = $created;
            // read your posted model attributes
            if ($model->load($request->post()) && $model->validate()) {
                $object = SCSPECIFICATIONPRICAT::find()->where([]);
                $model->save();
                return 'Saved';

            }
            else {
                return ActiveForm::validate($model);//['output'=>'Error', 'message'=>'Ошибка валидации'];
            }
        }

        // Else return to rendering a normal view
        return $this->render('specifications');
    }


    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Проверьте пожалуйста вашу почту, мы отправили вам инструкции.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Извините, попробуйте пожалуйста позже. Также вы можете обратиться к нашему системному администратору.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль успешно записан.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
